//
//  AppDelegate.h
//  booth
//
//  Created by fpm0259 on 2018/7/30.
//  Copyright © 2018年 fpm0259. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

